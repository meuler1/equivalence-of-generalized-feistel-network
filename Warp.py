
from sympy.combinatorics import SymmetricGroup
from math import factorial
import pickle
from EqClasses import *
from tqdm import tqdm
from MyPermutation import MyPermutation

"""
Enumerate variants of the permutation of WARP to find better ones
"""


OBJ_AS = 64

def generateWarp(N):
    print("Generation as in WARP")
    EO = []
    q = list(range(2 * N))
    for L in tqdm(SymmetricGroup(N).generate(),total=factorial(N)):
        LL = MyPermutation.double(L)
        for rot in range(2 * N):
            P1 = MyPermutation.getLBlock(LL,q[rot:]+q[:rot])
            EO.append(P1)
            
    print(f'{len(EO)} candidates')
    return EO

def generateWarp_Var1(N):
    print("Generation as in WARP  -  Variant 1")
    EO = []
    q = list(range(2 * N))
    for L in tqdm(SymmetricGroup(N).generate(),total=factorial(N)):
        LL = MyPermutation.double(L)
        for rot in range(2 * N):
            P1 = MyPermutation.getEvenOdd(MyPermutation(q[rot:]+q[:rot]),LL)
            EO.append(P1)
    print(f'{len(EO)} candidates')
    return EO

def generateWarp_Var2(N):
    print("Generation as in WARP - Variant 2")
    EO = []
    q = list(range(2 * N))
    for L in tqdm(SymmetricGroup(N).generate(),total=factorial(N)):
        LL = MyPermutation.double(L)
        for rot in range(2 * N):
            P1 = MyPermutation.getEvenOdd(LL,MyPermutation(q[rot:]+q[:rot]))
            EO.append(P1)
    print(f'{len(EO)} candidates')
    
    return EO

def generateWarp_Var3(N):
    print("Generation as in WARP - Variant 3")

    q = list(range(2 * N))
    with open(f"{N}-EqClasses",'rb') as file :
        E = pickle.load(file)
    E = [MyPermutation(P) for P in E]
    E = [P for P in E if P.get_DR(20)<=10]

    EO = []
    for rot in range(0,2*N):
        Rot = MyPermutation(q[rot:]+q[:rot])
        for rot2 in range(0,2*N):
            Rot2 = MyPermutation(q[rot2:]+q[:rot2])
            for P in tqdm(E) :
                newP = MyPermutation.getEvenOdd(MyPermutation.double(P.getL())*Rot,MyPermutation.double(P.getR())*Rot2)
                EO.append(newP)

    print(f'{len(EO)} candidates')
   
    
    return EO

def reduce_eq(E) :
    D = sortbyInv(E, extended=True)
    for invariant in tqdm(D) :
        D[invariant] = EquivalenceReduce(D[invariant], extended=False)
        D[invariant] = EquivalenceReduce(D[invariant], extended=True)
    redE = ExpandedEquivalence(D, extended=True)
    print(f'Reduced to {len(redE)} candidates')
    return redE
    
if __name__ == "__main__" :
    N=8
    E=[P for P in generateWarp(N) if P.get_DR()<=10]
    print(f'{len(E)} candidates with DR <=10')
    E= reduce_eq(E)
    total_time = 0

    for P in tqdm(E) :
        AS,t =  P.ActiveS(18,OBJ_AS,diff=True)
        if AS >= OBJ_AS :
            print(18,list(P),AS, P.get_DR(),P.is_expanded_eq(P.getDual()), P.is_expanded_eq(P.getDual().getInverse()))
        total_time+=t
    for P in tqdm(E) :
        AS,t =  P.ActiveS(19,OBJ_AS,diff=True)
        if AS >= OBJ_AS :
            print(19,list(P),AS, P.get_DR(),P.is_expanded_eq(P.getDual()), P.is_expanded_eq(P.getDual().getInverse()))
        total_time+=t
    print()
        
    E=[P for P in generateWarp_Var1(N) if P.get_DR()<=10]
    print(f'{len(E)} candidates with DR <=10')
    E= reduce_eq(E)
    total_time = 0
    for P in tqdm(E) :
        AS,t =  P.ActiveS(18,OBJ_AS,diff=True)
        if AS >= OBJ_AS :
            print(18,list(P),AS, P.get_DR(),P.is_expanded_eq(P.getDual()), P.is_expanded_eq(P.getDual().getInverse()))
        total_time+=t
    for P in tqdm(E) :
        AS,t =  P.ActiveS(19,OBJ_AS,diff=True)
        if AS >= OBJ_AS :
            print(19,list(P),AS, P.get_DR(),P.is_expanded_eq(P.getDual()), P.is_expanded_eq(P.getDual().getInverse()))
        total_time+=t
    print()

    E=[P for P in generateWarp_Var2(N) if P.get_DR()<=10]
    print(f'{len(E)} candidates with DR <=10')
    E= reduce_eq(E)
    total_time = 0
    for P in tqdm(E) :
        AS,t =  P.ActiveS(18,OBJ_AS,diff=True)
        if AS >= OBJ_AS :
            print(18,list(P),AS, P.get_DR(),P.is_expanded_eq(P.getDual()), P.is_expanded_eq(P.getDual().getInverse()))
        total_time+=t
    for P in tqdm(E) :
        AS,t =  P.ActiveS(19,OBJ_AS,diff=True)
        if AS >= OBJ_AS :
            print(19,list(P),AS, P.get_DR(),P.is_expanded_eq(P.getDual()), P.is_expanded_eq(P.getDual().getInverse()))
        total_time+=t
    print()    
    
    E=[P for P in generateWarp_Var3(N) if P.get_DR()<=10]
    print(f'{len(E)} candidates with DR <=10')
    E= reduce_eq(E)
    total_time = 0
    for P in tqdm(E) :
        AS,t =  P.ActiveS(18,OBJ_AS,diff=True)
        if AS >= OBJ_AS :
            print(18,list(P),AS, P.get_DR(),P.is_expanded_eq(P.getDual()), P.is_expanded_eq(P.getDual().getInverse()))
        total_time+=t
    for P in tqdm(E) :
        AS,t =  P.ActiveS(19,OBJ_AS,diff=True)
        if AS >= OBJ_AS :
            print(19,list(P),AS, P.get_DR(),P.is_expanded_eq(P.getDual()), P.is_expanded_eq(P.getDual().getInverse()))
        total_time+=t
    print()
