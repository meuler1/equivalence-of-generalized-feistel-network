from sympy.combinatorics import Permutation, SymmetricGroup
import numpy as np
import copy
from networkx import DiGraph
from networkx.algorithms.isomorphism import is_isomorphic, DiGraphMatcher
from Gurobi_Feistel import AS

"""
    All the functions needed to build and compare permutations

"""
class MyPermutation(Permutation):
    DR = None
    AS = None
    class_dual = None
    class_inv = None

    def __init__(self, L):
        super().__init__()
        
        
    ########## Construct permutations  ######################
    
    #Pi_{L,R}
    def getEvenOdd(L, R):
        P = []
        for x in range(L.size):
            P += [2 * L(x) + 1, 2 * R(x)]
        return MyPermutation(P)

    #Phi_{C,C}
    def getPair(C):
        P = []
        for x in range(C.size):
            P += [2 * C(x), 2 * C(x) + 1]
        return MyPermutation(P)
    

    # Concatenate twice a permutation
    def double(P):
        return Permutation(list(P) + [x + P.size for x in P])

    #\Psi_{alpha}^{R}
    def getAlphaR(alpha, R):
        return MyPermutation.getEvenOdd(alpha * R, R)

    def getInverse(self):
        return self ** (-1)
    
    # Dual permutation
    def getDual(self):
        assert self.is_evenodd()
        L = self.getL()
        R = self.getR()
        return MyPermutation.getEvenOdd(R, L)
    
    def getLBlock(P, rot):
        P = Permutation(P)
        Pinv = P ** (-1)
        rot = Permutation(rot)
        # l =  rot o Pinv
        # r = P
        return MyPermutation.getEvenOdd(Pinv * rot, P)
    
    
    
    #####################  Get properties of a permutation #############################"
    
    # Returns the  cycle structure of r rounds of the permutation as a tuple
    def get_cycle_structure(self, r):
        return tuple(sorted((self ** r).cycle_structure.items()))    

    # Permutation of the left branches
    def getL(self):
        return MyPermutation([x // 2 for x in list(self)[::2]])

    # Permutation of the right branches
    def getR(self):
        return MyPermutation([x // 2 for x in list(self)[1::2]])

    # alpha  = R^{-i} L^i
    def getAlpha(self,i):
        return (self.getL() **i)  * (self.getR() ** (-i))

    # compute Centr(R^i alpha R^{-i})
    def getS_perm(self):
        R = self.getR()
        alpha = self.getAlpha(1)
        return getS(R, alpha)
    

    # Matrix of the linear layer of a GFN
    def getM(self):
        M = np.zeros((self.size, self.size))
        for i in range(self.size):
            M[self(i)][i] = 1
            if i % 2 == 0:
                M[self(i + 1)][i] = 1
        M = np.matrix(M)
        return M

    # get the diffusion round of the GFN in the direct (encrypt) direction
    def get_DR_up(self, bound_dr=20):
        M = self.getM()
        base = 8
        N = copy.copy(M) ** base
        if np.count_nonzero(N) == self.size ** 2:
            base = 1
            N = copy.copy(M) ** base
        for i in range(0, bound_dr - base + 1):
            if np.count_nonzero(N) == self.size ** 2:
                return i + base
            N = M * N
        return 100

    # get the maximum of the diffusion round in both directions
    def get_DR(self, bound_dr=20):
        if self.DR is None:
            self.DR = max(self.get_DR_up(bound_dr), self.getInverse().get_DR_up(bound_dr))
        return self.DR

    # check if the permutation is even odd
    def is_evenodd(self):
        for x in range(self.size):
            if self(x) % 2 == x % 2:
                return False
        return True

    # check if the permutation is a permutation of  pair
    def is_Pair(self):
        return self.getL() == self.getR()


    # get the Cyclic Feistel Graph of length r
    def getGraph(self, r=2):
        G = DiGraph()
        for i in range(r):
            for j in range(self.size):
                G.add_edge(f"{i}_{j}", f"{(i + 1) % r}_{self(j)}")
                if j % 2 == 0:
                    G.add_edge(f"{i}_{j}", f"{(i + 1) % r}_{self(j + 1)}")
        return G
    
    def ActiveS(self,R,AS_bound,diff=True):        
            if diff :
                return AS(self,R,AS_bound)
            else :
                return AS(self.getDual(),R,AS_bound)
    
    def getAS(self,R,AS_bound,diff=True):
        if self.AS is None :
            self.AS = self.ActiveS(R,AS_bound,diff)[0]
        return self.AS
   


    ############# Isomorphismm #############################################################""
    # test if self and Perm are r-cyclic equivalent
    def is_cyclic_equivalent(self, Perm, r):
        G1 = self.getGraph(r=r)
        G2 = Perm.getGraph(r=r)
        return is_isomorphic(G1, G2)

    # if extended is True : test if self and Perm are r-cyclic equivalent
    # i  extended is True : test if self and Perm are extended r-cyclic equivalent
    def is_extended_cyclic_equivalent(self, Perm, r, extended=False):
        return self.is_cyclic_equivalent(Perm, r) or (extended and self.getInverse().is_cyclic_equivalent(Perm, r))

    # get the succesive permutations of pairs needed to make explicit the isomorphisme
    def getAi(self, Perm, r=2):
        Match = next(DiGraphMatcher(Perm.getGraph(r), self.getGraph(r)).isomorphisms_iter())
        A = {}
        for i in range(r):
            Matchi = {key: Match[key] for key in Match if key[0] == str(i)}
            A[i] = [None] * self.size
            for key in Matchi:
                A[i][int(key.split("_")[-1])] = int(Match[key].split("_")[-1])
            A[i] = Permutation(A[i])
        Ainv = {i: A[i] ** (-1) for i in range(r)}
        assert (MyPermutation([Ainv[0]((self ** r)(A[0](x))) for x in range(self.size)]) == Perm ** r)
        for i in range(r):
            assert (MyPermutation([Ainv[i]((self ** i)(A[0](x))) for x in range(self.size)]) == Perm ** i)
        return A
    
	
    
    def is_expanded_eq(self, Perm):
        if self.is_Pair():
            return Perm.is_Pair()
        
        S = self.getS_perm()
        for B in S.generate():
            BP = self * MyPermutation.getPair(B)
            if BP.get_cycle_structure(1) == Perm.get_cycle_structure(1) and  Perm.is_cyclic_equivalent(BP, r=1):
                return True
        return False
    
    


def getS(R, alpha):
    x = alpha
    D = [alpha]
    while R  * x * R ** (-1) not in D:
        x = R * x * R  ** (-1)
        D.append(x)
    return SymmetricGroup(R.size).centralizer(D)



