from Generation import generate_expanded_classes
from collections import Counter
from EqClasses import sortbyInv
import pickle


"""
Enumerate all the classes of even odd GFNs with less than 16 branches
"""

for k in range(1,8) :
    print(f"============= k= {k} ===========")
    E = generate_expanded_classes(k)
    for P in E :
        P.get_DR(20)
        1
    print("computed the DR")
    C = Counter(P.DR for P in E)
    print(C)
    if k>=5 :
        DRmin = min(C)
        E = [P for P in E if P.DR<= DRmin+3]
        print(f"DR <= {DRmin+3} : reduced to {len(E)} candidates")
        
    D= sortbyInv(E)
    for invariant in D :
        drup,drinv = invariant[0]
        invariant_inverse = ((drinv,drup),invariant[1])
        
        while len(D[invariant])>0 :
            P = D[invariant][0]
        
            if P.class_dual is None :
                P.class_dual = next(Q for Q in D[invariant_inverse] if Q.class_dual is None and P.getDual().is_expanded_eq(Q))
                if P.class_dual != P :
                    P.class_dual.class_dual = P
            if P.class_inv is None :
                P.class_inv = next(Q for Q in D[invariant_inverse] if Q.class_inv is None and  P.getInverse().is_expanded_eq(Q))
                if P.class_inv != P :
                    P.class_inv.class_inv = P
            D[invariant] = D[invariant][1:]

    
    for P in tqdm(E) :
        if P.AS is None and P.class_inv.AS is None :
            P.getAS(20,0)
            P.class_inv.AS = P.AS


    print("computed the AS",flush=True)      
    

    E = sorted(E,key=lambda x:x.DR)
    for i in range(len(E)) :
        if E.index(E[i].class_dual)>i+1 :
            E= E[:i+1]+ [E[i].class_dual] + [x for x in E[i+1:] if x!= E[i].class_dual ]

            
    for P in E :
        print(list(P),P.DR,P.AS,P.class_dual.AS, E.index(P.class_dual) ,E.index(P.class_inv))
    
    with open(f"{k}-EqClasses",'wb') as file :
        pickle.dump([list(P) for P in E],file)    
