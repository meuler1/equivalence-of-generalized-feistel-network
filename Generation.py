from sympy.combinatorics import SymmetricGroup
from MyPermutation import MyPermutation, getS
from collections import defaultdict
from time import time

"""
 How to generate efficiently sets of permutations :
    - Improve on CGT19 to deal with extended equivalence
    - Enumeration of one representative per conjugacy-based equivalence class
    - Enumeration of one representative per expanded equivalence class
"""


# Auxiliary function : return all the decomposition possible of n
def partitions(n: int, I=1):
    yield (n,)
    for i in range(I, n // 2 + 1):
        for p in partitions(n - i, i):
            yield (i,) + p

# generate A_N : one permutation per cycle decomposition type
def class_eq_L(N: int):
    for p in partitions(N):
        l_p = []
        m = 0
        for n in p:
            l_p += list(range(m + 1, m + n)) + [m]
            m += n
        yield MyPermutation(l_p)
        

def enumeration_CGT19(N) :
    classes = []
    for L in class_eq_L(N) :
        for R in SymmetricGroup(N).generate() :
            classes.append(MyPermutation.getEvenOdd(L,R))
    return classes

# Improve on CGT19 to deal with extended equivalence
def extended_enumeration(N) :
    classes = []
    MapS = defaultdict(list)
    for R in SymmetricGroup(N).generate() :
        MapS[MyPermutation(list(R)).get_cycle_structure(1)].append(R)
    SortedKeys = sorted([key for key in MapS], key= lambda x:len(MapS[x]))
    print([len(MapS[x]) for x in SortedKeys])
    for i,t in enumerate(SortedKeys):
        for cycle_type in SortedKeys[:i+1] :
            for R in MapS[cycle_type] :
                classes.append(MyPermutation.getEvenOdd(MapS[t][0],R))
    return classes



# Algorithm 1 : Enumeration of conjugacy-based equivalence classes
def generate_conjugacy_classes(N) :
    classes = []
    for t in class_eq_L(N) :
        S = set(SymmetricGroup(N).generate())
        C = SymmetricGroup(N).centralizer(t)
        while len(S) != 0 :
            R = S.pop()
            classes.append(MyPermutation.getEvenOdd(t,R))
            S.difference_update(C.conjugacy_class(R))
    return classes

# Algorithm 2 : Enumeration of expanded equivalence classes
def generate_expanded_classes(N) :
    classes = []
    for alpha in class_eq_L(N):
        S = set(SymmetricGroup(N).generate())
        C = SymmetricGroup(N).centralizer(alpha)
        while len(S)!=0 :
            R = S.pop()
            S.difference_update(C.conjugacy_class(R))
            for B in getS(R,alpha).generate():
                if R*B in S:
                    S.difference_update(C.conjugacy_class(R*B))
            classes.append(MyPermutation.getAlphaR(alpha,R))
    return classes


if __name__ == "__main__" :
    for N in range(5,10) :
        print("====== N = " ,N)
        t= time()
        A = enumeration_CGT19(N)
        print(f"CGT19 : {time()-t:.2f}s" )

        t=time()
        B = generate_conjugacy_classes(N)
        print(f"Alg1: {time()-t:.2f}s" )

        t=time()
        B = generate_expanded_classes(N)
        print(f"Alg2: {time()-t:.2f}s" )
