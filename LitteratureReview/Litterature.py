import sys

sys.path.append("..")

from MyPermutation import MyPermutation
from collections import Counter
from  EqClasses import *

def readline(line) :
    try :
        assert line[0]=="["
        L = line.strip()[1:-1].split(",\t")
        l = [ MyPermutation([int(a) for a in x[1:-1].split(',') ]) for x in L ]
        l = [P for P in l if P.is_evenodd()]
        return  l
    except UnboundLocalError as e:
        pass
    except Exception as e:
        print(f"Error with line {line.strip()} ",e)
    

def importFrom(filename) : 
    print(f"Import permutations from {filename}")
    with open(filename, 'r') as f:
        f.readline()
        lines = f.readlines()
        L = []
        for line in lines :
            L+= readline(line)
    return L
    
def test(filename):
    Perms = importFrom(filename)
    SizePerms = defaultdict(list)
    for P in Perms :
        SizePerms[P.size].append(P)
    for S in SizePerms :
        print(f"Permutations of size {S}")
        D = EquivalenceReduce(SizePerms[S])
        DD = EquivalenceReduce(SizePerms[S],extended = True)
        E = ExpandedEquivalence(SizePerms[S],verbose=True)
        EE = ExpandedEquivalence(E,extended=True,verbose=True)
        
        print(f"\t in the paper : {len(SizePerms[S])} , conjugacy based = {len(D)}, extended conjugacy based = {len(DD)}")
        print(f"\t Expanded classes : {len(E)}, Expanded Extended : {len(EE)}")
  
import glob

for filename in glob.glob("source_*.txt"):
    test(filename)
    
    
    