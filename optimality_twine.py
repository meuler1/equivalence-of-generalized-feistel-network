from Generation import generate_expanded_classes
from tqdm import tqdm
from EqClasses import *
from collections import Counter

"""
Enumerate of the 16 branches even odd GFN to prove that Twine is optimal
"""

N=8
E = generate_expanded_classes(N)
print(f"There are {len(E)} expanded classes of equivalence of even-odd {2*N}-branch permutations")
E = [P for P in tqdm(E) if P.get_DR() <12]
print(f"There are {len(E)} expanded classes of equivalence of even-odd {2*N}-branch permutations with DR <12" )
print("Regroup by extended equivalence")
redE = ExpandedEquivalence(E, extended=True)
print(f'Reduced to {len(redE)} candidates')
C = Counter(P.get_DR() for P in redE)
print("Repartiton of DR :")
for dr in C.keys() :
    print(f"\tThere are {C[dr]} extended expanded classes with DR={dr}")
keep = []

print("Test on 14 rounds")
for P in tqdm(redE) :
    P.AS = P.ActiveS(14,32) 
    if P.AS[0] >=32 :
        print(list(P), P.get_DR(), P.AS)
print("Test on 15 rounds")
for P in tqdm(redE) :
    P.AS = P.ActiveS(15,32) 
    if P.AS[0] >=32 :
        print(list(P), P.get_DR(), P.AS)