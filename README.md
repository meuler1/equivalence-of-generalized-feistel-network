This code is associated with the ToSC/FSE 2024 paper "Equivalence of Generalized Feistel Networks" by Patrick Derbez and Marie Euler.

It uses mainly Python 3 and its libraries sympy.combinatorics (for permutations) and gurobipy (for the MILP modeling).

