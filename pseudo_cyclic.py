from matplotlib import pyplot as plt
import matplotlib.ticker as plticker
from MyPermutation import *
from sympy.combinatorics import PermutationGroup
from sympy import isprime, totient
from math import gcd
from numpy.random import permutation


def fibonacci(k):
    if k == 0 or k == 1:
        return k
    else:
        return fibonacci(k-2)+fibonacci(k-1)

Max = 50
y = []
avg = []
for k in range(2, Max):
    alpha = MyPermutation(list(range(1, k))+[0])

    m = 30
    for i in range(1, k):
    
        j = k/gcd(i, k)
        R = MyPermutation([(x*i + (x//j)) % k for x in range(k)])
        P = MyPermutation.getAlphaR(alpha, R)
        dr = P.get_DR(bound_dr=m)
        if  dr <= m:
            m = dr
                
     
    y.append(m)

    n = 30
    for _ in range(1000):
        L = MyPermutation(permutation(k))
        R = MyPermutation(permutation(k))
        P = MyPermutation.getEvenOdd(L, R)
        if P.get_DR(bound_dr=n) < n:
            n = P.get_DR(bound_dr=m)

    avg.append(n)
    print(f"k={k}, best pseudo cyclic DR = {m}, best random DR ={n}")


def fibonacci_bound(k):
    i = 0
    while fibonacci(i) < k:
        i += 1
    return i+1


plt.rcParams["figure.figsize"] = (9, 5)

# plt.plot(range(2,Max),y, marker='o',linestyle='None')
fibo = [fibonacci_bound(x) for x in range(1, Max)]
debruijn = [(16, 8), (32, 10), (64, 11), (128, 13)]

loc = plticker.MultipleLocator(base=4)
fig,ax=plt.subplots()
#ax.xaxis.set_major_locator(loc)
ax.xaxis.set_ticks([0,4,8,12,16,20,30,40,50,60,70,80,100,120,140])
ax.plot(range(1, Max), fibo, linestyle='dotted',
         label='Fibonacci lower bound')
ax.plot(list(range(2, Max)), avg[:Max-2], linestyle='None',marker='.',
         label='Min of 1000 random even-odd permutations',color='purple')
ax.plot([d[0]//2 for d in debruijn], [d[1] for d in debruijn],
         marker='s', linestyle='None', markersize=8, label='De Bruijn graphs',color='red')

ax.plot([x for x in range(2, Max) if isprime(x)], [y[x-2] for x in range(2, Max) if isprime(x)],
         marker='o', markersize=8, linestyle='None', label='Pseudo-cyclic (prime k)')
ax.plot([x for x in range(2, Max) if not isprime(x)], [y[x-2] for x in range(2, Max)
         if not isprime(x)], marker='*', linestyle='None', label='Pseudo-cyclic (other k)')


plt.xlabel("Number of pairs of branches k")
plt.ylabel("Diffusion round")
plt.legend()


plt.show()

