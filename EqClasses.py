from collections import defaultdict

"""
 Auxiliary functions to reduce list according to an equivalence relation
"""

# regroup the permutations of L according to a list of class invariants
def sortbyInv(L, extended=False):
    E = defaultdict(list)
    for x in L:
        if extended:
            E[tuple([tuple(sorted([x.get_DR_up(), x.getInverse().get_DR_up()])),
                     tuple([x.getAlpha(i).get_cycle_structure(1) for i in range(1,10)])])].append(x)
        else:
            E[((x.get_DR_up(), x.getInverse().get_DR_up()),
               tuple([x.getAlpha(i).get_cycle_structure(1) for i in range(1,10)]))].append(x)
    return E


# reduce a list according to conjugacy-based equivalence
def EquivalenceReduce(L, extended=False):
    out = []
    for x in L:
        if not extended:
            if not any(map(lambda c: c.getL().get_cycle_structure(1) == x.getL().get_cycle_structure(1)
                                     and c.getR().get_cycle_structure(1) == x.getR().get_cycle_structure(1)
                                     and c.is_extended_cyclic_equivalent(x, extended=False, r=1), out)):
                out.append(x)
        else:
            if not any(map(lambda c: c.is_extended_cyclic_equivalent(x, extended=True, r=1), out)):
                out.append(x)
    return out

# reduce a list according to expanded-equivalence
def ExpandedEquivalence(E, extended=False,verbose=False):
    E = sortbyInv(E, extended=extended)
    redE = []
    for dr, alpha_c in sorted(list(E.keys()), key=lambda x: len(E[x])):
        if len(alpha_c[0]) == 1 and alpha_c[0][0][0] == 1:
            redE.append(E[(dr, alpha_c)][0])
        else:
            remains = E[(dr,alpha_c)]
            while len(remains) > 0:
                P = remains[0]
                for Q in list(remains):
                    if P.is_expanded_eq(Q) or (extended and Q.is_expanded_eq(P**(-1))) :
                        remains.remove(Q)
                        if verbose and Q!=P:
                            r=1
                            while not P.is_extended_cyclic_equivalent(Q,extended=extended,r=r) :
                                r+=1
                            print(r, list(P),list(Q) )
                            if extended and P.is_extended_cyclic_equivalent(Q**(-1),extended= False ,r=r):
                               print("extended",P.getAi(Q**(-1),r=r))
                            else :
                                print(P.getAi(Q,r=r))
                redE.append(P)
    return redE
