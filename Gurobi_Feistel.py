import gurobipy as gp

"""
Model to compute the minimal number of active S-boxes in a differential/linear trail
"""

gp.setParam("LogToConsole", 1)


# We model the truncated XOR by excluding the three impossibles cases 
def XOR(m, a, b):
    c = m.addVar(vtype="b")         # c = a + b
    m.addConstr((1-a)+b+c >= 1)     # a = 1 , b = 0,  c = 0
    m.addConstr(a+(1-b)+c >= 1)     # a = 0 , b = 1,  c = 0
    m.addConstr(a+b+(1-c) >= 1)     # a = 0 , b = 0,  c = 1
    return c

# modelisation of the propagation of differences in a GFN with permutation P in R rounds
# We use a bound to stop prematurely the computation if the result is not satisfactory (AS < AS_bound)
# It only deals with even-odd permutations
def getModel(P, R, AS_bound):
    P = list(P)
    m = gp.Model()
    state = [m.addVar(vtype='b') for _ in range(len(P))]
    obj = sum(state[::2])
    m.addConstr(obj >= 1)
    for r in range(R-1):
        new_state = []
        for i in range(len(P)):
            if i % 2 == 1:
                new_state.append(state[P.index(i)])   # Right branches gets previous left branches
            else:
                new_state.append(XOR(m, state[P.index(i)-1], state[P.index(i)]))
                
        state = new_state
        obj += sum(state[::2]) # only active left branches count
        
    m.setObjective(obj)
    m.__AS_bound = AS_bound
    return m


# We use a bound to stop prematurely the computation if the result is not satisfactory (AS
def mycallback(model, where):
    if where == gp.GRB.Callback.MIPSOL:
        # MIP solution callback
        obj = model.cbGet(gp.GRB.Callback.MIPSOL_OBJ)
        if round(obj) < model.__AS_bound:
            model.terminate()



# computation of the minimal number of active Sboxes in R rounds of a GFN with permutation P
def AS(P, R, AS_bound):
    m = getModel(P, R, AS_bound)
    m.optimize(mycallback)
    return round(m.ObjVal), m.runTime
